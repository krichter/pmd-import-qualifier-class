package de.richtercloud.pmd.importt.qualifier.clazz;

import static de.richtercloud.pmd.importt.qualifier.clazz.SomeOtherClass.*;

public class SomeClass {

    public void theMethod() {
        someMethod();
        Object someObject = new Object();
        if(someObject instanceof SomeOtherClass.SomeInnerClass) {
            System.out.println("");
        }
    }
}
