package de.richtercloud.pmd.importt.qualifier.clazz;

public class SomeOtherClass {

    public static void someMethod() {
        System.out.println("someMethod");
    }

    protected class SomeInnerClass {

        public void alsoDoSomething() {
            System.out.println("alsoDoSomething");
        }
    }
}
